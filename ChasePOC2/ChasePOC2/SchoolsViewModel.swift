//
//  SchoolsViewModel.swift
//  ChasePOC2
//
//  Created by Sai Lagisetty on 3/14/23.
//

import Foundation

class SchoolsViewModel: ObservableObject {
    @Published var schools: [School] = []
    @Published var satScores: [SATDetails] = []
    @Published var isLoading: Bool = true
    @Published var selectedSchoolSatScore: SATDetails?
    @Published var noSatScoreAvailable = false
    
    func getSchools() {
        isLoading = true
        let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
        SchoolAPIService.shared.fetchSchools(from: url) { [weak self] (result: Result<[School], APIRequestError>) in
            
            defer {
                self?.getSchoolsSAT()
            }
            
            switch result {
            case .success(let schools):
                self?.schools = schools
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getSchoolsSAT() {
        let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!
        SchoolAPIService.shared.fetchSATScores(from: url) { [weak self] (result: Result<[SATDetails], APIRequestError>) in
            
            defer {
                self?.isLoading = false
            }
            
            switch result {
            case .success(let satScores):
                self?.satScores = satScores
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func selectedSchool(dbn: String) {
        guard let score = self.satScores.first(where: { $0.dbn == dbn }) else {
            self.noSatScoreAvailable = true
            return
        }
        self.selectedSchoolSatScore = score
    }
}
