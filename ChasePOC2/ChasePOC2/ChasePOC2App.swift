//
//  ChasePOC2App.swift
//  ChasePOC2
//
//  Created by Sai Lagisetty on 3/14/23.
//

import SwiftUI

@main
struct ChasePOC2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
